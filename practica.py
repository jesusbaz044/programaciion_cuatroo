import redis 
import datetime

r = redis.Redis()

def agregarPalabra():
    nuevapalabra = input("Ingrese la nueva palabra: ")
    nuevosignificado = input("Ingrese su significado: ")
    r.hset("diccio", nuevapalabra, nuevosignificado)
    r.set(nuevapalabra, nuevosignificado)

def buscarSignificado():
    palavra = input("Ingrese la palabra que desea saber su significado: ")
    print(r.get(palavra))
def mostrarPalabras():
      print(r.hgetall("diccio"))

repetir_bucle = True
while repetir_bucle:
    print("Diccionario de Panamá")
    print("1.agregar palabra")
    print("2.Mostrar palabras")
    print("3.Buscar significado")
    print("4.Salir")
    
    opc= int(input("Que desea hacer?: "))
    if opc== 1:
        agregarPalabra() 
    elif opc==2: 
        mostrarPalabras()
    elif opc == 3:
        buscarSignificado()
    elif opc ==4:
        repetir_bucle= False
    else:
        print("Opcion incorrecta")

print("Gracias por usar nuestros servicios")
